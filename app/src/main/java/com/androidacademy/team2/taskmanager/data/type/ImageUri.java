package com.androidacademy.team2.taskmanager.data.type;

import java.io.Serializable;

public class ImageUri implements CharSequence, Serializable{
    static final long serialVersionUID = -6876076024254298227L;

    private String uri;

    public ImageUri(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public int length() {
        return this.uri.length();
    }

    @Override
    public char charAt(int index) {
        return this.uri.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return this.subSequence(start, end);
    }

    @Override
    public String toString() {
        return this.uri;
    }
}
