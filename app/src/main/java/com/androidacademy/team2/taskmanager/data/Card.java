package com.androidacademy.team2.taskmanager.data;

public class Card extends Note
{
    public Card(Object content) {
        super(content);
    }

    public Card(String title, Object content) {
        super(title, content);
    }

    public Card(String id, String title, Object content) {
        super(id, title, content);
    }
}