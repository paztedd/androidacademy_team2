package com.androidacademy.team2.taskmanager.data;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.androidacademy.team2.taskmanager.R;
import com.androidacademy.team2.taskmanager.activities.EditNoteActivity;
import com.androidacademy.team2.taskmanager.data.type.ImagePath;
import com.androidacademy.team2.taskmanager.data.type.ImageUri;
import com.squareup.picasso.Picasso;

import java.util.List;

public class NoteAdapter extends RecyclerView.Adapter<NoteAdapter.NoteHolder> {
    private static final int TEXT_VIEW_TYPE = 0;
    private static final int IMAGE_VIEW_TYPE = 1;
    private static final int IMAGE_PATH_VIEW_TYPE = 2;
    private static final int LINK_VIEW_TYPE = 3;

    private ItemsRepository repository;
    private final OnItemClickListener clickListener;

    public NoteAdapter(ItemsRepository repository, OnItemClickListener clickListener) {
        this.repository = repository;
        this.clickListener = clickListener;
    }

    public void setRepository(ItemsRepository repository) {
        this.repository = repository;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public NoteHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        final NoteHolder holder;

        switch (viewType) {
            case TEXT_VIEW_TYPE:
                holder = new TextNoteHolder(inflater.inflate(R.layout.text_note, parent, false));
                break;
            case IMAGE_VIEW_TYPE:
                holder = new ImageNoteHolder(inflater.inflate(R.layout.image_note, parent, false));
                break;
            case IMAGE_PATH_VIEW_TYPE:
                holder = new ImagePathNoteHolder(inflater.inflate(R.layout.image_note, parent, false));
                break;
            default:
                throw new IllegalArgumentException("Unsupported view type: " + viewType);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.onItemClick(repository.getNotes().get(holder.getAdapterPosition()));
                }
            }
        });
        return holder;
    }

    @NonNull
    private NoteHolder getNoteHolder(@NonNull ViewGroup parent, int viewType, LayoutInflater inflater) {
        View noteView = inflater.inflate(R.layout.text_note, parent, false);

        final NoteHolder holder;
        switch (viewType) {
            case  TEXT_VIEW_TYPE:
                holder = new TextNoteHolder(noteView);
                break;
            case IMAGE_VIEW_TYPE:
                holder = new ImageNoteHolder(noteView);
                break;
            case IMAGE_PATH_VIEW_TYPE:
                holder = new ImagePathNoteHolder(noteView);
                break;
            default:
                holder = new TextNoteHolder(noteView);
        }

        noteView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (clickListener != null) {
                    clickListener.onItemClick(repository.getNotes().get(holder.getAdapterPosition()));
                }
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull NoteHolder holder, int position) {
        final Note note = repository.getNotes().get(position);
        holder.bind(note);
    }

    @Override
    public int getItemCount() {
        return repository.getNotes().size();
    }

    @Override
    public int getItemViewType(int position) {
        Object content = repository.getNotes().get(position).getContent();

        // todo: parse image uri
        if (content instanceof ImageUri) {
            return IMAGE_VIEW_TYPE;
        }

        if(content instanceof ImagePath){
            return IMAGE_PATH_VIEW_TYPE;
        }
        /*if (content instanceof Uri) {
            return linkViewType;
        }*/
        return TEXT_VIEW_TYPE;
    }

    public void addNote(Note note){
        List<Note> notes = repository.getNotes();
        notes.add(note);
        notifyItemInserted(notes.size() - 1);
    }

    public void addNote(Note note, int position) {
        repository.getNotes().add(note);
        notifyItemInserted(position);
    }

    public Note removeNote(int position) {
        Note removed = repository.getNotes().remove(position);
        notifyItemRemoved(position);
        return removed;
    }

    public Note getNote(int position) {
        return  repository.getNotes().get(position);
    }

    public interface OnItemClickListener {
        void onItemClick(@NonNull Note note);
    }

    public static abstract class NoteHolder<T> extends RecyclerView.ViewHolder {
        public RelativeLayout viewForeground;
        public RelativeLayout viewBackground;

        public ImageView deleteIcon;
        public ImageView moveIcon;

        private final TextView title;

        public NoteHolder(View view) {
            super(view);

            viewBackground = view.findViewById(R.id.view_background);
            viewForeground = view.findViewById(R.id.view_foreground);

            deleteIcon = view.findViewById(R.id.delete_icon);
            moveIcon = view.findViewById(R.id.move_icon);

            title = view.findViewById(R.id.title);
        }

        public void bind(Note<T> note) {
            title.setText(note.getTitle());
        }
    }

    static class TextNoteHolder extends NoteHolder<String> {
        private final TextView content;

        public TextNoteHolder(View view) {
            super(view);
            content = view.findViewById(R.id.editText);
        }

        @Override
        public void bind(Note<String> note) {
            super.bind(note);
            content.setText(note.getContent());
        }
    }

    static class ImageNoteHolder extends NoteHolder<ImageUri> {
        private final ImageView content;

        public ImageNoteHolder(View view) {
            super(view);
            content = view.findViewById(R.id.image);
        }

        @Override
        public void bind(Note<ImageUri> note) {
            super.bind(note);
            Picasso.get().load(Uri.parse(note.getContent().getUri())).into(content);
        }
    }

    static class ImagePathNoteHolder extends NoteHolder<ImagePath> {
        private final ImageView content;

        public ImagePathNoteHolder(View view) {
            super(view);
            content = view.findViewById(R.id.image);
        }

        @Override
        public void bind(Note<ImagePath> note) {
            super.bind(note);
            Picasso.get().load(Uri.parse(note.getContent().getPath())).into(content);
        }
    }

    // todo: web link preview
    /*static class LinkNoteHolder extends NoteHolder<Uri> {
        private final ImageView content;

        public LinkNoteHolder(View view) {
            super(view);
            content = view.findViewById(R.id.image);
        }

        @Override
        public void bind(Note<Uri> note) {
            super.bind(note);
            Picasso.get().load(note.getContent()).into(content);
        }
    }*/
}
