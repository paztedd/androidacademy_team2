package com.androidacademy.team2.taskmanager.data;

import java.io.Serializable;

public class Note<T> implements Serializable {
    static final long serialVersionUID = 7534337067265729856L;

    protected String id;
    protected final String title;
    protected final T content;

    public Note(T content) {
        this.id = "0";
        this.title = "";
        this.content = content;
    }

    public Note(String title, T content) {
        this.id = "0";
        this.title = title;
        this.content = content;
    }

    public Note(String id, String title, T content) {
        this.id = id;
        this.title = title;
        this.content = content;
    }

    public static <T> Note<T> create(String title, T content) {
        return new Note<>(title, content);
    }

    public String getTitle() {
        return title;
    }

    public T getContent() {
        return content;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
