package com.androidacademy.team2.taskmanager.data;

import java.util.ArrayList;
import java.util.List;

public class ItemsRepository {
    private final List<Note> notes;

    public ItemsRepository() {
        this.notes = new ArrayList<Note>();
    }

    public ItemsRepository(List<Note> notes) {
        this.notes = notes;
    }

    public List<Note> getNotes() {
        return notes;
    }
}
