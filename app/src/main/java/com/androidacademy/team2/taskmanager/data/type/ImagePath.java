package com.androidacademy.team2.taskmanager.data.type;

import java.io.Serializable;

public class ImagePath implements CharSequence, Serializable{
    static final long serialVersionUID = -6876076024254298227L;

    private String path;

    public ImagePath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    @Override
    public int length() {
        return this.path.length();
    }

    @Override
    public char charAt(int index) {
        return this.path.charAt(index);
    }

    @Override
    public CharSequence subSequence(int start, int end) {
        return this.subSequence(start, end);
    }

    @Override
    public String toString() {
        return path;
    }
}
