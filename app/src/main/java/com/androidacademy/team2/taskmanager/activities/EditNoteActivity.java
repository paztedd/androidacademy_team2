package com.androidacademy.team2.taskmanager.activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.androidacademy.team2.taskmanager.R;
import com.androidacademy.team2.taskmanager.data.ItemsRepository;
import com.androidacademy.team2.taskmanager.data.Note;
import com.androidacademy.team2.taskmanager.data.NoteAdapter;
import com.androidacademy.team2.taskmanager.data.NoteModel;
import com.androidacademy.team2.taskmanager.helper.Database;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class EditNoteActivity extends AppCompatActivity {

    private static final String NOTE_ID = "noteId";

    EditText title;
    EditText content;
    Button save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_note);

        title = (EditText) findViewById(R.id.title);
        content = (EditText) findViewById(R.id.content);
        save = (Button) findViewById(R.id.save);

        Database.getRef("backlog")
                .addValueEventListener(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot != null) {
                                    DataSnapshot item = dataSnapshot.child(getNoteId());

                                    if (item != null) {
                                        NoteModel noteModel = item.getValue(NoteModel.class);

                                        if (noteModel != null) {
                                            Note note = noteModel.getNoteObject();

                                            title.setText(note.getTitle());
                                            content.setText((String) note.getContent());
                                        }
                                    }
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                // Getting Post failed, log a message
                                Log.w("Database", "loadPost:onCancelled", databaseError.toException());
                                // ...
                            }
                        }

                );

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                save();
            }
        });
    }

    public void save() {
        DatabaseReference databaseReference = Database.getRef("backlog");

        NoteModel noteModel = new NoteModel(new Note<String>(getNoteId(), title.getText().toString(), content.getText().toString()));

        databaseReference.child(getNoteId()).setValue(noteModel)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        backToBackLog();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Write failed
                        // ...
                    }
                });

    }

    private String getNoteId() {
        return this.getIntent().getStringExtra(NOTE_ID);
    }

    private void backToBackLog() {
        BacklogActivity.start(this, true);
    }

    public static void start(Context context, Note note) {
        Intent intent = new Intent(context, EditNoteActivity.class);

        intent.putExtra(NOTE_ID, note.getId());

        context.startActivity(intent);
    }
}
