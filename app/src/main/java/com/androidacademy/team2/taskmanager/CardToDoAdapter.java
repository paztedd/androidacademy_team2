package com.androidacademy.team2.taskmanager;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidacademy.team2.taskmanager.data.Card;
import com.squareup.picasso.Picasso;

public class CardToDoAdapter extends ArrayAdapter<Card> {

    public CardToDoAdapter(Context context) {
        super(context, 0);
    }

    @Override
    public View getView(int position, View contentView, ViewGroup parent) {
        CardHolder holder;

        if (contentView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            contentView = inflater.inflate(R.layout.item_card, parent, false);
            holder = new CardHolder(contentView);
            contentView.setTag(holder);
        } else {
            holder = (CardHolder) contentView.getTag();
        }

        Card card = getItem(position);

        holder.name.setText(card.getTitle());
        //Glide.with(getContext()).load(spot.url).into(holder.image);

        if (card.getContent() instanceof String) {

        } else {
            Picasso.get().load(card.getContent().toString()).into(holder.image);
        }

        return contentView;
    }

    static class CardHolder {

        public ImageView image;
        public TextView name;

        public CardHolder(View itemView) {
            this.image = itemView.findViewById(R.id.item_card_image);
            this.name = itemView.findViewById(R.id.item_card_name);
        }
    }
}
