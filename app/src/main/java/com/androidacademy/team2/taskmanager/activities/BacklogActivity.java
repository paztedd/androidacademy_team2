package com.androidacademy.team2.taskmanager.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidacademy.team2.taskmanager.CardToDoAdapter;
import com.androidacademy.team2.taskmanager.R;
import com.androidacademy.team2.taskmanager.ToDoStack;
import com.androidacademy.team2.taskmanager.data.Card;
import com.androidacademy.team2.taskmanager.data.ItemsRepository;
import com.androidacademy.team2.taskmanager.data.Note;
import com.androidacademy.team2.taskmanager.data.NoteAdapter;
import com.androidacademy.team2.taskmanager.data.NoteModel;
import com.androidacademy.team2.taskmanager.data.RecyclerItemTouchHelper;
import com.androidacademy.team2.taskmanager.data.type.ImagePath;
import com.androidacademy.team2.taskmanager.helper.Database;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import static android.support.v7.widget.helper.ItemTouchHelper.LEFT;
import static android.support.v7.widget.helper.ItemTouchHelper.RIGHT;

public class BacklogActivity extends AppCompatActivity implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private FirebaseAuth mAuth;

    private RecyclerView recyclerView;

    private TextView selectedNoteTitle;

    private NoteAdapter.OnItemClickListener clickListener;

    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_backlog);

        selectedNoteTitle = findViewById(R.id.selected_note);
        handler = new Handler();
        initRecyclerView();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fabAddNew = (FloatingActionButton) findViewById(R.id.fabAddNew);
        fabAddNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTextNote();
            }
        });

        FloatingActionButton fabToStackView = (FloatingActionButton) findViewById(R.id.fabToStackView);
        fabToStackView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startStackView();
            }
        });

        Database.getRef("backlog")
                .addValueEventListener(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot != null) {
                                    NoteAdapter noteAdapter = (NoteAdapter) recyclerView.getAdapter();

                                    ItemsRepository repository = new ItemsRepository();

                                    for (DataSnapshot item : dataSnapshot.getChildren()) {
                                        NoteModel noteModel = item.getValue(NoteModel.class);

                                        if (noteModel != null) {
                                            repository.getNotes().add(noteModel.getNoteObject());
                                        }
                                    }

                                    noteAdapter.setRepository(repository);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                // Getting Post failed, log a message
                                Log.w("Database", "loadPost:onCancelled", databaseError.toException());
                                // ...
                            }
                        }
                );

        Intent intent = getIntent();
        String action = intent.getAction();
        String type = intent.getType();

        if(intent.ACTION_SEND.equals(action) && type != null){
            DatabaseReference ref = Database.getRef("backlog");
            if("text/plain".equals(type)){
                handleSendText(intent, ref);
            } else if(type.startsWith("image/")){
                handleSendImage(intent, ref);
            }
        }
    }

    private void handleSendText(Intent intent, DatabaseReference ref) {
        String sharedText = intent.getStringExtra(intent.EXTRA_TEXT);
        if(sharedText != null) {
            NoteModel noteModel = new NoteModel(new Note("#" + "shared", sharedText));
            ref.child(noteModel.getId()).setValue(noteModel);
        }
    }

    private void handleSendImage(Intent intent, DatabaseReference ref) {
        Uri sharedImage = intent.getParcelableExtra(intent.EXTRA_STREAM);
        if(sharedImage != null) {
            String parsedPath = sharedImage.toString();
            NoteModel noteModel = new NoteModel(new Note("#" + "shared img", new ImagePath(parsedPath) ));
            ref.child(noteModel.getId()).setValue(noteModel);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        mAuth = FirebaseAuth.getInstance();

        FirebaseUser currentUser = mAuth.getCurrentUser();

        if (currentUser == null) {
            LoginActivity.start(this);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_back_log, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initRecyclerView() {
        recyclerView = findViewById(R.id.backlog_items);
        RecyclerView.LayoutManager layoutManager;

        int columnCount = getResources().getInteger(R.integer.column_count);
        if (columnCount > 1) {
            layoutManager = new StaggeredGridLayoutManager(columnCount, StaggeredGridLayoutManager.VERTICAL);
        } else {
            layoutManager = new LinearLayoutManager(this);
        }
        recyclerView.setLayoutManager(layoutManager);

        clickListener = new NoteAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(@NonNull Note note) {
                editNote(note);
            }
        };

        ItemsRepository repository = new ItemsRepository(); // todo: fill from storage
        final NoteAdapter noteAdapter = new NoteAdapter(repository, clickListener);
        recyclerView.setAdapter(noteAdapter);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, LEFT | RIGHT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);
    }

    private void editNote(Note note) {
        EditNoteActivity.start(this, note);
    }

    public void logout(MenuItem item) {
        LoginActivity.logout(mAuth, this);
    }

    public static void start(Context context, boolean isClearHistory) {
        Intent intent = new Intent(context, BacklogActivity.class);

        if (isClearHistory) {
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        }

        context.startActivity(intent);
    }

    private void addTextNote() {
        NoteAdapter noteAdapter = (NoteAdapter) recyclerView.getAdapter();
        final int index = noteAdapter.getItemCount() + 1;

        DatabaseReference ref = Database.getRef("backlog");

        final Note newNote = new Note("#" + index, "");
        NoteModel noteModel = new NoteModel(newNote);

        ref.child(noteModel.getId()).setValue(noteModel);
        editNote(newNote);

        handler.post(new Runnable() {
            @Override
            public void run() {
                recyclerView.smoothScrollToPosition(index);
                editNote(newNote);
            }
        });
    }

    private Note removeNote(RecyclerView.ViewHolder viewHolder, int position) {
        NoteAdapter noteAdapter = (NoteAdapter) recyclerView.getAdapter();

        String nodeId = noteAdapter.getNote(position).getId();

        Note removed = noteAdapter.removeNote(viewHolder.getAdapterPosition());

        DatabaseReference refBacklog = Database.getRef("backlog");

        refBacklog.child(nodeId).removeValue();
        return removed;
    }

    private void addToStack(Note note){
        DatabaseReference ref = Database.getRef("stack");

        Card card = new Card(note.getId(), note.getTitle(), note.getContent());

        NoteModel noteModel = new NoteModel(card);
        ref.child(noteModel.getId()).setValue(noteModel);

        DatabaseReference refBacklog = Database.getRef("backlog");

        refBacklog.child(noteModel.getId()).removeValue();
    }

    private void startStackView(){
        ToDoStack.start(this);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        switch (direction) {
            case LEFT:
                removeNote(viewHolder, position);
                break;
            case RIGHT:
                Note removed = removeNote(viewHolder, position);
                addToStack(removed);
                break;
        }
    }

    @Override
    public void onDrag(RecyclerView.ViewHolder viewHolder, int direction) {
        ImageView deleteIcon = ((NoteAdapter.NoteHolder) viewHolder).deleteIcon;
        ImageView moveIcon = ((NoteAdapter.NoteHolder) viewHolder).moveIcon;
        deleteIcon.setVisibility(direction == RIGHT ? View.INVISIBLE : View.VISIBLE);
        moveIcon.setVisibility(direction == RIGHT ? View.VISIBLE : View.INVISIBLE);
    }
}
