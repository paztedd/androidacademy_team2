package com.androidacademy.team2.taskmanager.data;

import android.provider.ContactsContract;
import android.util.Base64;
import android.util.Log;

import com.androidacademy.team2.taskmanager.ToDoStack;
import com.androidacademy.team2.taskmanager.helper.Database;
import org.apache.commons.lang3.SerializationUtils;

public class NoteModel {
    private String id = null;
    private String note = null;

    public NoteModel() {
    }

    public NoteModel(String id, String note) {
        this.id = id;
        this.note = note;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public NoteModel(Note note) {
        this.note = Base64.encodeToString(SerializationUtils.serialize(note), Base64.DEFAULT);
        this.id = note.getId().equals("0") ? Database.crc32(this.note) : note.getId();
        note.setId(this.id);
        this.note = Base64.encodeToString(SerializationUtils.serialize(note), Base64.DEFAULT);
    }

    public Note getNoteObject() {
        return SerializationUtils.deserialize(Base64.decode(this.note, Base64.DEFAULT));
    }

    public NoteModel(Card card) {
        this.note = Base64.encodeToString(SerializationUtils.serialize(card), Base64.DEFAULT);
        this.id = card.getId().equals("0") ? Database.crc32(this.note) : card.getId();
        card.setId(this.id);
        this.note = Base64.encodeToString(SerializationUtils.serialize(card), Base64.DEFAULT);
    }

    public Card getCardObject() {
        Note note = SerializationUtils.deserialize(Base64.decode(this.note, Base64.DEFAULT));

        if (note instanceof Note) {
            note = new Card(note.getId(), note.getTitle(), note.getContent());
        }

        return (Card)note;
    }
}
