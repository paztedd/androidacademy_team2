package com.androidacademy.team2.taskmanager;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.androidacademy.team2.taskmanager.activities.BacklogActivity;
import com.androidacademy.team2.taskmanager.data.Card;
import com.androidacademy.team2.taskmanager.data.NoteModel;
import com.androidacademy.team2.taskmanager.helper.Database;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;
import com.yuyakaido.android.cardstackview.CardStackView;
import com.yuyakaido.android.cardstackview.SwipeDirection;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ToDoStack extends AppCompatActivity {
    private List<Card> cardsToChoose = new ArrayList<>();

    private List<Card> DayCards;
    private List<Card> WeekCards;
    private List<Card> MonthCards;


    public static void start(BacklogActivity backlogActivity) {
        Intent intent = new Intent(backlogActivity, ToDoStack.class);

        backlogActivity.startActivity(intent);
    }

    private CardStackView cardStackView;
    private CardToDoAdapter adapter;

    private boolean manualAnimation = false;
    private boolean processingSwipe = false;
    private boolean switchingActivity = false;

    private Card currentCard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_to_do_stack);

        DayCards = new ArrayList();
        WeekCards = new ArrayList();
        MonthCards = new ArrayList();

        Button dayBtn = findViewById(R.id.dayButton);
        dayBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(processingSwipe ) {
                    return;
                }
                processingSwipe = true;

                swipeRight();
                DayCards.add(currentCard);
                cardsToChoose.remove(currentCard);
            }
        });

        Button weekBtn = findViewById(R.id.weekButton);
        weekBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(processingSwipe ) {
                    return;
                }
                processingSwipe = true;

                swipeRight();
                DayCards.add(currentCard);
                cardsToChoose.remove(currentCard);
            }
        });

        Button monthBtn = findViewById(R.id.monthButton);
        monthBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(processingSwipe ) {
                    return;
                }

                processingSwipe = true;

                swipeRight();
                DayCards.add(currentCard);
                cardsToChoose.remove(currentCard);
            }
        });

        Database.getRef("stack")
                .addValueEventListener(
                        new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot != null) {
                                    cardsToChoose.clear();
                                    cardsToChoose = new ArrayList<>();

                                    for (DataSnapshot item : dataSnapshot.getChildren()) {
                                        NoteModel noteModel = item.getValue(NoteModel.class);

                                        if (noteModel != null) {
                                            cardsToChoose.add(noteModel.getCardObject());
                                        }
                                    }

                                   adapter.addAll(cardsToChoose);
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                // Getting Post failed, log a message
                                Log.w("Database", "loadPost:onCancelled", databaseError.toException());
                                // ...
                            }
                        }
                );

//        cardsToChoose = createDefaultCards();
        setup();
        reload();
    }

    private void setup() {

        cardStackView = (CardStackView) findViewById(R.id.ToDoCardStack);
        //int bottomPadding = cardStackView.getHeight() - findViewById(R.id.item_card_image).getHeight();
        //cardStackView.setPadding(
        //        cardStackView.getPaddingLeft(),
        //        cardStackView.getPaddingTop(),
        //        cardStackView.getPaddingRight(),
        //        bottomPadding);
        cardStackView.setCardEventListener(new CardStackView.CardEventListener() {
            @Override
            public void onCardDragging(float percentX, float percentY) {
                Log.d("CardStackView", "onCardDragging");
                if(!switchingActivity && adapter != null && adapter.getCount() != 0 && cardStackView != null) {
                    int index = cardStackView.getTopIndex();
                    if(adapter.getCount() > index) {
                        currentCard = adapter.getItem(index);
                    }
                }
            }

            @Override
            public void onCardSwiped(SwipeDirection direction) {
                Log.d("CardStackView", "onCardSwiped: " + direction.toString());
                Log.d("CardStackView", "topIndex: " + cardStackView.getTopIndex());
                //if (adapter.getCount() == 0) {
                //    Log.d("CardStackView", "Paginate: " + cardStackView.getTopIndex());
                //    paginate();
                //}
                processingSwipe = false;
                cardStackView.setRightOverlay(R.layout.refresh_image);

                if(direction == SwipeDirection.Left)
                {
                    sendToBacklog();
                }

                List<Card> cards = extractRemainingCards();
                if (cards.isEmpty()) {
                    if(cardsToChoose.size() == 0) {
                        switchingActivity = true;
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                OpenToDoActivity();
                            }
                            }, 1000);

                    }
                    else {
                        // TODO: hack to avoid null's
                        for(int i =0; i < cardsToChoose.size(); i++){
                            if(cardsToChoose.get(i) == null) {
                                cardsToChoose.remove(i);
                                i--;
                            }
                        }

                        setup();
                        reload();
                    }
                }
                manualAnimation = false;
            }

            @Override
            public void onCardReversed() {
                Log.d("CardStackView", "onCardReversed");
            }

            @Override
            public void onCardMovedToOrigin() {
                Log.d("CardStackView", "onCardMovedToOrigin");
            }

            @Override
            public void onCardClicked(int index) {
                Log.d("CardStackView", "onCardClicked: " + index);
            }
        });
    }

    private void sendToBacklog()
    {
        //int index = cardStackView.getTopIndex();
        cardsToChoose.remove(currentCard);
    }

    private void OpenToDoActivity() {
        Intent intentToDo = new Intent(this, ToDoActivity.class);
        startActivity(intentToDo);
    }

    private CardToDoAdapter createCardsAdapter() {
        final CardToDoAdapter adapter = new CardToDoAdapter(getApplicationContext());
        adapter.addAll(cardsToChoose);
        return adapter;
    }

    private List<Card> createDefaultCards() {
        List<Card> cards = new ArrayList<>();
//        cards.add(new Card("Yasaka Shrine", "https://source.unsplash.com/Xq1ntWruZQI/600x800"));
//        cards.add(new Card("Fushimi Inari Shrine", "https://source.unsplash.com/NYyCqdBOKwc/600x800"));
//        cards.add(new Card("Bamboo Forest", "https://source.unsplash.com/buF62ewDLcQ/600x800"));
//        cards.add(new Card("Brooklyn Bridge", "https://source.unsplash.com/THozNzxEP3g/600x800"));
//        cards.add(new Card("Empire State Building", "https://source.unsplash.com/USrZRcRS2Lw/600x800"));
//        cards.add(new Card("The statue of Liberty", "https://source.unsplash.com/PeFk7fzxTdk/600x800"));
//        cards.add(new Card("Louvre Museum", "https://source.unsplash.com/LrMWHKqilUw/600x800"));
//        cards.add(new Card("Eiffel Tower", "https://source.unsplash.com/HN-5Z6AmxrM/600x800"));
//        cards.add(new Card("Big Ben", "https://source.unsplash.com/CdVAUADdqEc/600x800"));
//        cards.add(new Card("Great Wall of China", "https://source.unsplash.com/AWh9C-QjhE4/600x800"));
        return cards;
    }

    private void paginate() {
        cardStackView.setPaginationReserved();
        adapter.addAll(cardsToChoose);
        adapter.notifyDataSetChanged();
    }

    private void reload() {
//        cardStackView.setVisibility(View.GONE);
//        new Handler().postDelayed(new Runnable() {
//            @Override
//            public void run() {
                adapter = createCardsAdapter();
                cardStackView.setAdapter(adapter);
//                cardStackView.setVisibility(View.VISIBLE);
//            }
//        }, 1000);
    }

    private LinkedList<Card> extractRemainingCards() {
        LinkedList<Card> cards = new LinkedList<>();
        for (int i = cardStackView.getTopIndex(); i < adapter.getCount(); i++) {
            cards.add(adapter.getItem(i));
        }
        return cards;
    }

    public void swipeRight() {
        List<Card> cards = extractRemainingCards();
        if (cards.isEmpty()) {
            return;
        }
        manualAnimation = true;
        currentCard = adapter.getItem(cardStackView.getTopIndex());
        Log.d("tag", "swiped: " + currentCard.getTitle());

        cardStackView.setRightOverlay(0);

        View target = cardStackView.getTopView();
        View targetOverlay = cardStackView.getTopView().getOverlayContainer();

        ValueAnimator rotation = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("rotation", 10f));
        rotation.setDuration(200);
        ValueAnimator translateX = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("translationX", 0f, 2000f));
        ValueAnimator translateY = ObjectAnimator.ofPropertyValuesHolder(
                target, PropertyValuesHolder.ofFloat("translationY", 0f, 500f));
        translateX.setStartDelay(100);
        translateY.setStartDelay(100);
        translateX.setDuration(500);
        translateY.setDuration(500);
        AnimatorSet cardAnimationSet = new AnimatorSet();
        cardAnimationSet.playTogether(rotation, translateX, translateY);

        ObjectAnimator overlayAnimator = ObjectAnimator.ofFloat(targetOverlay, "alpha", 0f, 1f);
        overlayAnimator.setDuration(200);
        AnimatorSet overlayAnimationSet = new AnimatorSet();
        overlayAnimationSet.playTogether(overlayAnimator);

        cardStackView.swipe(SwipeDirection.Right, cardAnimationSet);
    }


}
